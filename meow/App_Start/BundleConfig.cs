﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace meow.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/admin/styles")
                .Include("~/content/styles/adminStyle.css")
                .Include("~/content/styles/adminPlugins.bundle.css")
                .Include("~/content/styles/adminDatatables.bundle.css")
                );

            bundles.Add(new StyleBundle("~/distributor/styles")
                .Include("~/content/styles/distributorStyle.css")
                .Include("~/content/styles/distributorPlugins.bundle.css")
                );

            bundles.Add(new StyleBundle("~/styles")
                .Include("~/content/styles/generalStyle.css")
                .Include("~/content/styles/generalPlugins.bundle.css")
                .Include("~/content/styles/login.css")
                );

            bundles.Add(new ScriptBundle("~/admin/scripts")
                .Include("~/scripts/adminPlugins.bundle.js")
                .Include("~/scripts/adminScripts.bundle.js")
                .Include("~/scripts/adminDashboard.js")
                .Include("~/scripts/adminDatatable.js")
                .Include("~/scripts/adminDatatables.bundle.js")
                .Include("~/areas/admin/scripts/forms.js")
                );

            bundles.Add(new ScriptBundle("~/distributor/scripts")
                .Include("~/scripts/distributorPlugins.bundle.js")
                .Include("~/scripts/distributorScripts.bundle.js")
                .Include("~/scripts/distributorDashboard.js")
                );

            bundles.Add(new ScriptBundle("~/scripts")
                .Include("~/scripts/generalPlugins.bundle.js")
                .Include("~/scripts/generalScripts.bundle.js")
                .Include("~/scripts/generalDashboard.js")
                .Include("~/scripts/login.js")
                );
        }
    }
}