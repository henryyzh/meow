﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace meow.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class InvoicesController : Controller
    {
        // GET: Admin/Invoices
        public ActionResult Index()
        {
            return View();
        }
    }
}