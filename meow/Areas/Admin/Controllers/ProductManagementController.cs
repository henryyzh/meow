﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace meow.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class ProductManagementController : Controller
    {
        // GET: Admin/ProductManagement
        public ActionResult Index()
        {
            return View();
        }
    }
}