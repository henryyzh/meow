﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace meow.Areas.Distributor.Controllers
{
    [Authorize(Roles = "distributor")]
    public class DashboardController : Controller
    {
        // GET: Distributor/Dashboard
        public ActionResult Index()
        {
            return View();
        }
    }
}