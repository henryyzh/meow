﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace meow.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (User.IsInRole("admin"))
                return RedirectToAction("Index", "Dashboard", new { area = "Admin" });
            else if (User.IsInRole("distributor"))
                return RedirectToAction("Index", "Dashboard", new { area = "Distributor" });
            else 
                return View();
        }

        public ActionResult Error()
        {
            return View();
        }



    }
}